<?php
/**
 * Created by Faisal Ijaz.
 * User: Faisal
 * Date: 7/8/18
 * Time: 10:29 AM
 */

$lang['statement_receipt_details'] = 'Receipt (%s) ';
$lang['report_invoice_city_'] = "City";
$lang['report_invoice_state_'] = "State";

$lang['client_select_title'] = 'Select Client';
$lang['slip_number'] = 'Slip No';
$lang['receipt_amount'] = 'Amount';
$lang['receipt_date'] = 'Date';
$lang['client_invoice_number_table_heading'] = 'Invoice #';
$lang['client_amount_table_heading'] = 'Amount';
$lang['client_status_table_heading'] = 'Status';
$lang['note_description'] = 'Note description';

$lang['receipt_type'] = 'Type';
$lang['receipt_trnxn_no'] = 'Transcation No';
$lang['receipt_cheque_date'] = 'Cheque Date';
$lang['receipt_currency'] = 'Currency';
$lang['receipt_deposited_status'] = 'Deposited Status';
$lang['receipt_deposited_verified'] = 'Deposited Verified';
$lang['receipt_note'] = 'Note';
$lang['invoice_payment_table_number_heading'] = 'Payment';
$lang['receipt_details'] = 'Receipt Details';
$lang['payment_details'] = 'Payment Details';
$lang['receipt_deposited'] = 'Deposited';
$lang['receipt_handover'] = 'Handover';
$lang['receipt_verified'] = 'Verified';
$lang['receipt_created'] = 'Created';
$lang['invoice_select_owner'] = 'Owner';
$lang['owner_select_title'] = 'Select Owner';
$lang['receipt_number'] = 'Receipt Number';
$lang['receipt_created_by'] = 'Created By';
$lang['receipt_status'] = 'Status';
$lang['receipt_handover_title'] = 'Handover';
$lang['receipt_deposit_title'] = 'Deposit';
$lang['receipt_verify_title'] = 'Verify';
$lang['advance_available_text'] = 'Available Adnvance Cash';
$lang['receipt_total'] = 'Total Payable';
$lang['receipt_due_amount'] = 'Amount Due';
$lang['total_payable_title'] = 'Total Payable';
$lang['receipt_cheque_date'] = 'Cheque Date';
$lang['receipt_cheque_number'] = 'Cheque Number';
$lang['client_name'] = 'Client Name';
$lang['bank_info'] = 'Bank';

$lang['receipt_send_to_client_modal_heading'] = "Send receipt to client";
$lang['receipt_send_to_client_attach_pdf'] = "Attach Receipt PDF";
$lang['email_template_receipts_fields_heading'] = 'Receipts';
$lang['invoice_date'] = 'Date';
$lang['staff_edit_email_signature_image'] = 'Email Signature Iamge';

$lang['update_note_custom'] = 'Edit Note';
$lang['add_note_custom'] = 'Add Note';
$lang['add_note_custom_type'] = 'Type';
$lang['add_note_custom_position'] = "Position";
$lang['add_note_custom_title'] = "Title";
$lang['title_listings'] = "Listing";
$lang['invoice_dt_table_heading_accepted_status'] = "Approval Status";
$lang['invoice_dt_table_heading_accepted_status_actions'] = "Actions";
$lang['customer_services'] = "Services";
$lang['custome_note'] = 'Extra Details';
$lang['customer_services_groups'] = 'Service Group';
$lang['customer_permission_receipts'] = 'Receipts';
$lang['vat_number'] = 'VAT #';
$lang['invoice_item_type'] = "Type";

$lang['invoice_single_mark_as_unpaid'] = 'Mark as Unpaid';
$lang['invoice_marked_as_cancelled_successfully'] = 'Invoice marked as unpaid successfully';

$lang['settings_notifications_general'] = 'Reminder Notification Settings';
$lang['settings_notifications_tasks'] = 'Tasks';
$lang['settings_notifications_renewal'] = 'Renewals';
$lang['settings_tasks_lead_creation_remind_call'] = "When Lead is created remind to call";
$lang['settings_notifications_first_reminder'] = 'First Reminder';
$lang['settings_notifications_second_reminder'] = 'Second Reminder';
$lang['settings_notifications_third_reminder'] = 'Third Reminder';
$lang['show_tasks_reminders_on_calendar'] = "Show Tasks Reminder";
$lang['settings_tasks_lead_creation_due_date_auto'] = 'Set due date for automatically assigned tasks';

$lang['sync_invoices'] = "Sync Invoices";
$lang['invoice_add_edit_sync_from_date'] = "Start Date";
$lang['invoice_add_edit_sync_to_date'] = "End Date";
$lang['zoho_api'] = "ZOHO API Settings";

$lang['settings_zoho_client_id'] = "Client ID";
$lang['settings_zoho_client_secret'] = "Client Secret";
$lang['settings_zoho_redirect_uri'] = "Authorized redirect URI (must be same given in app creation)";
$lang['settings_zoho_auth_code'] = "CODE";
$lang['settings_zoho_access_token'] = "Access Token";
$lang['settings_zoho_organization_id'] = "Organization ID";

$lang['invoice_type'] = "Invoice Type";
$lang['invoice_type_performa'] = "Performa Invoice";
$lang['invoice_type_tax'] = "Tax Invoice";
$lang["settings_notifications"] = "Notifications settings";
$lang['settings_sales_performa_invoice_prefix'] = "Performa Invoice Perfix";
$lang["performa_invoice_prefix"] = "faisal@800wisdom.ae";
$lang["invoice_pdf_heading_perform"] = "Performa Invoice";
$lang['receipt_change_to_tax_invoice'] = "Change to Tax Invoice";
$lang['convert_performa_to_invoice'] = "chage to invoice";

# Invoice General

# Invoice General
$lang['invoice_status_paid']                = 'Paid';
$lang['invoice_status_unpaid']              = 'Unpaid';
$lang['invoice_status_overdue']             = 'Overdue';
$lang['invoice_status_not_paid_completely'] = 'Partially Paid';

$lang['invoice_pdf_heading'] = 'Tax Invoice';
$lang['invoice_pdf_heading_perform'] = 'Proforma Invoice';
$lang['proposal_pdf_heading'] = 'PROPOSAL';
$lang['lead_addedfrom'] = 'Created By';
$lang['invoice_dt_table_heading_id'] = "ID";
$lang['report_invoice_city']  = "City";
$lang['report_invoice_Country']  = "Country";
$lang['report_invoice_Country']  = "Country";
$lang['leads_dt_created_by'] = 'Created By';